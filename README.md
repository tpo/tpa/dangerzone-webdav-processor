The Dangerzone WebDAV processor is a program that will process files
on a WebDAV server with [dangerzone](https://dangerzone.rocks/) (only the "server-side" of it)
and render sanitized versions of untrusted files.

It is used by the Tor Project to sanitized files sent during hires.

# Abandoned project

This project has been abandoned, see [TPA-RFC-78][].

[TPA-RFC-78]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-78-dangerzone-retirement

# Usage

*This section assumes some administrator already has the processor
setup under the `dangerzone-bot` user, see the Installation section
below for information on how to set that up.*

Say you are someone receiving resumes or whatever untrusted content
and you actually *need* to open those files because that's part of
your job. What do you do?

 1. I make a folder in Nextcloud

 2. I upload the untrusted file(s) in the folder

 3. I share the folder with the `dangerzone-bot` user

 4. after a short delay, the file *disappears* (gasp! do not worry,
    they actually are moved to the `dangerzone-processing/` folder)

 5. then after another delay, if "dangerzone" succeeded, sanitized
    appear in a `safe/` folder and the original files are moved into a
    `dangerzone-processed/` folder, telling me they have been correctly
    processed.

 6. if that didn't work, they end up in `dangerzone-rejected/` and no
    new file appear in the `safe/` folder

# Installation

## Requirements

To deploy this on your infrastructure (or setup a development
environment), you will need the following requirements:

 * an account on a Nextcloud server (your own)
 * (optionally) a second account on the nextcloud server (for the bot)
 * a running Docker server that you have access to (on Debian, that
   means being part of the `docker` group), which can fetch (or have a
   local copy of) the [dangerzone-converter](https://github.com/firstlookmedia/dangerzone-converter) image
 * a `python3` command
 * the [webdavclient][] Python library (`python3-webdavclient` in
   Debian)

[webdavclient]: https://pypi.org/project/webdavclient/

## Nextcloud app password setup

Once you have all the above requirements, you need to do this in
Nextcloud.

This section is actually optional: you can use your own account, with
the normal password, but that's not recommended: you can setup
application-specific passwords and you should.

First, if you have admin access to Nextcloud, create a
`dangerzone-bot` account to process the content shared by other
users. It can be named something else, and you can use your own
account as well, but it's preferable to use a role account in case you
go away. (The point of this, after all, is that this stops depending
on you.)

Then login to Nextcloud using the role account (if created above) or
your own account and setup an "app password" (optional, but strongly
recommended):

 1. click on your avatar on the top-right
 2. pick the `Security` tab
 3. scroll down to `Devices & sessions`
 4. fill in `dangerzone-webdav-processor` as an App name
 5. hit `Create new app password`
 6. copy the password (it will disappear in the next step)
 7. hit `Done`

You now have a role account (or your own account) setup with a
password specific for your bot.

## Running the script by hand

Once you have a role account and password (or if you like to live
dangerously and just want to use your own account and password), you
can start processing *all* the files at the root of that account
using:

    export WEBDAV_PASSWORD=[REDACTED]
    ./dangerzone-webdav-processor --username dangerzone-bot  --location https://example.com/remote.php/dav/files/dangerzone-bot/ -v

Obviously, the `--username` and `--location` parameters must be
adapted to your configuration. The latter, in particular, can be found
in the `Files` dialog, under `Settings` (at the bottom left), in the
`WebDAV` section.

The above will process *all* folders (except the special ones) and, on
success, dump the sanitized files in the `safe/` special folder.

The above can be done in a loop to continually process files, but
obviously a better deployment mechanism is preferred, see below for
systemd support.

## Running under systemd

As can be seen above, the processor is expected to be ran every time
there are new files to process. A simple way to do this is to deploy
the systemd unit files provided along the script.

First install the script in a standard location, for example:

    install dangerzone-webdav-processor /usr/bin/

Note that you can also deploy the script with `pip3 install .` but in
that case you will need to change the path in the `.service` file.

Then add a role user for the project, grant it access to the Docker
daemon:

    adduser --system dangerzone
    adduser dangerzone docker

If you want to use another username, you will need to modify the
`.service` file.

Add the configuration file to
`/etc/default/dangerzone-webdav-processor`, in the following syntax:

    WEBDAV_LOCATION=https://example.com/remote.php/dav/files/dangerzone-bot/
    WEBDAV_USERNAME=dangerzone-bot
    WEBDAV_PASSWORD=[REDACTED]
    ARGS=--verbose

Notice how you can pass extra arguments to the systemd service in the
`ARGS` variable.

Those are equivalent to the `--location`, `--username`, and
`--password` parameters, documented above.

Then deploy the systemd scripts units:

    cp dangerzone-webdav-processor.{timer,service} /etc/systemd/system/
    systemctl daemon-reload
    systemctl enable dangerzone-webdav-processor.{timer,service}
    systemctl start dangerzone-webdav-processor.timer

You can then run the service by hand with:

    systemctl start dangerzone-webdav-processor.service

Or wait for the timer to expire (one hour). In any case, you can see
the output of the job with:

    systemctl status dangerzone-webdav-processor.service

# Contributing

See the [contribution guide](CONTRIBUTING.md) for more information on how to
participate or get support for this project. In short: this is a free
software project and you are welcome to join us in improving it, both
by fixing things, reporting issues or documentation.

# Copyright

[AGPLv3](LICENSE).

# Credits

This work is heavily based on [dangerzone][], written by [Micah
Lee](https://micahflee.com/). This is also an implementation of the [upstream web service
idea](https://github.com/firstlookmedia/dangerzone/issues/110) which is still under discussion.

[dangerzone]: https://dangerzone.rocks/

# History & Design

This documentation has been moved to the [TPA service page](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/dangerzone).
